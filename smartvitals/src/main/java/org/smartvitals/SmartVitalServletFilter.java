package org.smartvitals;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;

public class SmartVitalServletFilter implements Filter {
    private String CONTEXT = "/smartvitals/";
    private HashMap<String, String> map = new HashMap<String, String>();

    public void init(FilterConfig filterConfig) throws ServletException {
        Enumeration<String> parameters = filterConfig.getInitParameterNames();
        while(parameters.hasMoreElements()){
            String param = parameters.nextElement();
            String confvalue = filterConfig.getInitParameter(param);
            map.put(param, confvalue);
        }
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest)servletRequest).getSession(false);
        HttpServletRequest req = (HttpServletRequest)servletRequest;
        System.out.println(req.getRequestURI());
        if(req.getRequestURI().compareTo("/smartvitals") == 0){
            if(session != null){
                session.invalidate();
                session = null;
            }
        }
        if(session == null && req.getRequestURI().compareTo(CONTEXT + "services/ihealth") != 0) {
            servletResponse.setContentType("text/html");
            String uri = "http://52.37.117.211/smartvitals/services/ihealth";
            String url = "https://api.ihealthlabs.com:8443/OpenApiV2/OAuthv2/userauthorization/?client_id=400d695294644427b803083118a68b88&response_type=code&redirect_uri="
                    + URLEncoder.encode(uri, "UTF-8")
                    + "&APIName=OpenApiBP+OpenApiBG+OpenApiUserInfo&RequiredAPIName=OpenApiBP+OpenApiBG+OpenApiUserInfo&IsNew=true";
            ((HttpServletResponse) servletResponse).setStatus(((HttpServletResponse) servletResponse).SC_MOVED_TEMPORARILY);
            ((HttpServletResponse) servletResponse).setHeader("Location", url);
            ((HttpServletResponse) servletResponse).sendRedirect(((HttpServletResponse) servletResponse).encodeRedirectURL(url));
        }else{
            if(session == null && req.getRequestURI().compareTo(CONTEXT + "services/ihealth") == 0) {
                filterChain.doFilter(servletRequest, servletResponse);
            }else {
                if (req.getRequestURI().compareTo("/smartvitals/services/ihealth") != 0) {
                    String uri = ((HttpServletRequest) servletRequest).getRequestURI();
                    String context = ((HttpServletRequest) servletRequest).getContextPath();
                    uri = uri.replaceFirst(context + "//", "");
                    if (uri.trim().isEmpty()) {
                        String url = context + "/smartvitalsindex.jsp";
                        ((HttpServletResponse) servletResponse).setStatus(((HttpServletResponse) servletResponse).SC_MOVED_TEMPORARILY);
                        ((HttpServletResponse) servletResponse).setHeader("Location", url);
                        ((HttpServletResponse) servletResponse).sendRedirect(((HttpServletResponse) servletResponse).encodeRedirectURL(url));
                    } else {
                        filterChain.doFilter(servletRequest, servletResponse);
                    }
                } else {
                    if (session != null) {
                        boolean reqForwarded = false;
                        Object appCxt = req.getHeader("APPSCONTEXT");
                        if (appCxt == null && req.getRequestURI().indexOf(CONTEXT + "apps") >= 0) {
                            reqForwarded = true;
                            String ur = req.getRequestURI();
                            int index = req.getRequestURI().indexOf(CONTEXT + "apps/");
                            String urpast = ur.substring(index + 1);
                            int nindex = urpast.indexOf("/");
                            String appname = urpast.substring(0, nindex);
                            ((HttpServletResponse) servletResponse).addHeader("APPSCONTEXT", appname);
                            req.getRequestDispatcher(CONTEXT + map.get(appname));
                        } else {
                            if (appCxt != null) {

                            }
                            reqForwarded = true;
                        }
                        if (!reqForwarded) {
                            filterChain.doFilter(servletRequest, servletResponse);
                        }
                    }
                }
            }
        }
    }

    public void destroy() {

    }
}
