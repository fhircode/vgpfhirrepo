package org.smartvitals;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.json.JSONObject;
import org.smartvitals.fhirmapper.FHIRMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

@Path("/")
public class SmartVitalsService {
    private static Logger logger = Logger.getLogger(SmartVitalsService.class);
    private static String CLIENT_SECRET = "9bf819f8c94d43d9a2fee697a6c4b699";
    private static String CLIENT_ID = "400d695294644427b803083118a68b88";
    private static String CLIENT_ID_PARAM = "?client_id=" + CLIENT_ID;
    private String API_PREFIX = "https://api.ihealthlabs.com:8443/openapiv2/";
    private String CALLBACKURI = "http://52.37.117.211/smartvitals/services/ihealth";
    private String REDIRECTURI_ST = "redirect_uri";
    private String BP_SV = "9e6bfb2d29cc4ab68b04101418540862";
    private String BP_SV_PARAM = "&sv=" + BP_SV;
    private String BP_SC = "885dd1648f344213b153df34e00d024b";
    private String BP_SC_PARAM = "&sc=" + BP_SC;
    private String BASEURI = "https://api.ihealthlabs.com:8443/OpenApiV2/OAuthv2/userauthorization/";
    private String LOCALE = "&locale=en_US";

    @GET
    @Produces("text/plain")
    @Path("/testing")
    public String testing() {
        return "testing Vishal.";
    }


    private String getHttpGet(String url) throws IOException {
        HttpGet getRequest = new HttpGet(url);
        HttpClient httpClient = HttpClients.createDefault();
        HttpResponse serviceResponse = null;
        serviceResponse = httpClient.execute(getRequest);
        HttpEntity entity = serviceResponse.getEntity();
        String cclJSON = EntityUtils.toString(entity);
        return cclJSON;
    }

    @GET
    @Produces("text/plain")
    @Path("/Patient/{id}")
    public String patient(@PathParam("id") String id, @Context HttpServletRequest req, @Context HttpServletResponse res) {
        try {
            String url = urlToGetPatientInfo(req.getSession());
            HttpGet getRequest = new HttpGet(url);
            HttpClient httpClient = HttpClients.createDefault();
            getRequest.setHeader("CALLPURPOSE", "PATIENTINFO");
            HttpResponse serviceResponse = null;
            serviceResponse = httpClient.execute(getRequest);
            HttpEntity entity = serviceResponse.getEntity();
            String cclJSON = EntityUtils.toString(entity);
            return cclJSON;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "{}";
    }

    private String getAccessToken( AppDetails appDetails){
        return  "&access_token=" + appDetails.getAccessToken();
    }

    private String urlToGetPatientInfo(HttpSession session) throws UnsupportedEncodingException, JSONException {
        AppDetails details = AppDetails.getDetailsFromSession(session);
        String uri = CALLBACKURI;
        return API_PREFIX + "user/" + details.getUserID() + ".json/" + CLIENT_ID_PARAM +
                "&client_secret=" + CLIENT_SECRET + "&redirect_uri=" +
                URLEncoder.encode(uri, "UTF-8") + getAccessToken(details) + BP_SC_PARAM + "&sv=0dedf8d157e74fc0b1ed8d54d5a8eccc" + LOCALE;
    }

    @GET
    @Produces("text/plain")
    @Path("/ihealth")
    public String ihealthcallback(@QueryParam("code") String code, @Context HttpServletRequest req, @Context HttpServletResponse res) {
        System.out.print(code);
        HttpSession session = req.getSession(false);
        if (session == null) {
            String uri = CALLBACKURI;
            String url = null;
            try {
                url = "https://api.ihealthlabs.com:8443/OpenApiV2/OAuthv2/userauthorization/?client_id=" + CLIENT_ID +
                        "&client_secret=" + CLIENT_SECRET + "&grant_type=authorization_code&redirect_uri=" +
                        URLEncoder.encode(uri, "UTF-8") + "&code=" + URLEncoder.encode(code, "UTF-8") + "&client_para=xxx";
                String cclJSON = getHttpGet(url);
                session = req.getSession(true);// Create session
                session.setAttribute("APPDETAILS", cclJSON);
                session.setAttribute("iHEALTHCODE", code);
                session.setAttribute("SESSIONSTARTED", true);
              /*  PatientContext pc = new PatientContext();
                session.setAttribute("PATIENT", );*/
                res.setStatus(res.SC_MOVED_TEMPORARILY);
                url = req.getContextPath() + "/smartvitalsindex.jsp";
                res.setHeader("Location", url);
                res.sendRedirect(res.encodeRedirectURL(url));
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            String callpurpose = res.getHeader("CALLPURPOSE");
            if(callpurpose != null){
                if(callpurpose.compareTo("PATIENTINFO") == 0){
                    System.out.println("dasdas");
                }
            }
        }
        return "success";
    }

    @GET
    @Produces("text/plain")
    @Path("/Observation")
    public String getBloodPressure(@QueryParam("identifier") String identifier, @Context HttpServletRequest req) {
        int daysBack = 7;
        String[] codes = identifier.split("\\|");
        if(codes[1].compareTo("55284-4") == 0) {/*blood pressure*/
            HttpSession session = req.getSession();
            try {
                Calendar stcalendar = Calendar.getInstance();
                long startTime = stcalendar.getTimeInMillis();
                Date now = new Date();
                Date dt = new Date(now.getTime() - (daysBack * (1000 * 60 * 60 * 24)));
                stcalendar.set(dt.getYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes(), dt.getSeconds());
                AppDetails details = AppDetails.getDetailsFromSession(session);
                long sttime = stcalendar.getTimeInMillis() / 1000L;
                String stValue = "1342007425";
                long anothervalue = Instant.now().getEpochSecond() - (daysBack * 60 * 60 * 24);
                String anvalue = String.valueOf(anothervalue);
                String url = API_PREFIX + "user/" + details.getUserID() + "/bp.json?client_id=" + CLIENT_ID +
                        "&client_secret=" + CLIENT_SECRET + "&" + REDIRECTURI_ST + "=" + CALLBACKURI + "&access_token=" + details.getAccessToken() +
                        "&start_time=" + anvalue + "&end_time=" + Instant.now().getEpochSecond() + "&page_index=1&sc=" + BP_SC + "&sv=" + BP_SV;
                String cclJSON = getHttpGet(url);
                JSONObject jsonObject = new JSONObject(cclJSON);
                org.json.JSONArray jsonArray = jsonObject.getJSONArray("BPDataList");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject item = jsonArray.getJSONObject(i);
                    java.lang.Double bp = item.getDouble("LP");
                }
                String returnData = "{";
                returnData = returnData + "\"raw\":" + cclJSON + ",";
                StringBuilder builder = new StringBuilder("[");
                org.json.JSONArray jArray = jsonObject.getJSONArray("BPDataList");
                int length = jArray.length();
                for (int i = 0; i < length; i++) {
                    if (i > 0) {
                        builder.append(",");
                    }
                    builder.append(FHIRMapper.bp(jArray.getJSONObject(i)));
                }
                builder.append("]");
                returnData = returnData + "\"fhirdata\":" + builder.toString() + "}";
                return returnData;
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "{}";
    }
}

