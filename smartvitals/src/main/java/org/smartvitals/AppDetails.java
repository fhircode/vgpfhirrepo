package org.smartvitals;

import com.sun.jersey.core.impl.provider.entity.XMLJAXBElementProvider;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.servlet.http.HttpSession;

/**
 * Created by fhir on 3/25/2016.
 */
public class AppDetails {
    private String apiname;
    private String accessToken;
    private String expires;
    private String refreshToken;

    public String getApiname() {
        return apiname;
    }

    public void setApiname(String apiname) {
        this.apiname = apiname;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getRefreshTokenExpires() {
        return refreshTokenExpires;
    }

    public void setRefreshTokenExpires(String refreshTokenExpires) {
        this.refreshTokenExpires = refreshTokenExpires;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    private String refreshTokenExpires;
    private String userID;

    public static AppDetails getDetailsFromSession(HttpSession session) throws JSONException {
        String obj = (String)session.getAttribute("APPDETAILS");
        JSONObject jsonObject = new JSONObject(obj);
        AppDetails appdetails = new AppDetails();
        appdetails.setAccessToken(jsonObject.getString("AccessToken"));
        appdetails.setApiname(jsonObject.getString("APIName"));
        appdetails.setExpires(jsonObject.getString("Expires"));
        appdetails.setRefreshToken(jsonObject.getString("RefreshToken"));
        appdetails.setRefreshTokenExpires(jsonObject.getString("RefreshTokenExpires"));
        appdetails.setUserID(jsonObject.getString("UserID"));
        return appdetails;
    }

    public String getDetailsInfo(){
        return "{" +
            "USERID" + ":" + this.getUserID() +
        "}";
    }
}


