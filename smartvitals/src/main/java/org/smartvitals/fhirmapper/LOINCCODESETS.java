package org.smartvitals.fhirmapper;

/**
 * Created by fhir on 3/28/2016.
 */
public enum LOINCCODESETS {
    BP_DIASTOLIC("8462-4");
    private LOINCCODESETS(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    private String code;
}
