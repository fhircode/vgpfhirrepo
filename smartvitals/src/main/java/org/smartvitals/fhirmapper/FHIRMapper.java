package org.smartvitals.fhirmapper;


import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import ca.uhn.fhir.model.dstu2.composite.QuantityDt;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.valueset.ObservationStatusEnum;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONException;

public class FHIRMapper{
    public static String bp(org.json.JSONObject inputJson) throws JSONException {
        Observation observation = new Observation();
        observation.setStatus(ObservationStatusEnum.FINAL);

        Observation.Component systolic = observation.addComponent();
        CodingDt coding = systolic.getCode().addCoding();
        coding.setCode("8480-6").setSystem("http://loinc.org").setDisplay("Systolic blood pressure");

        QuantityDt value = new QuantityDt();
        value.setValue(inputJson.getInt("HP")).setSystem("").setCode("mm[Hg]");
        systolic.setValue(value);

        Observation.Component diastolic = observation.addComponent();
        CodingDt dcoding = diastolic.getCode().addCoding();
        dcoding.setCode("8462-4").setSystem("http://loinc.org").setDisplay("Diastolic blood pressure");

        QuantityDt dvalue = new QuantityDt();
        dvalue.setValue(inputJson.getInt("LP")).setSystem("").setCode("mm[Hg]");
        diastolic.setValue(dvalue);

        return FhirContext.forDstu2().newJsonParser().encodeResourceToString(observation);
    }

}
