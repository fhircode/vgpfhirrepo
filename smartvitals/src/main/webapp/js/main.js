$(function(){

    $.ajax("/smartvitals/services/Observation?identifier=http://loinc.org|55284-4").done(function(data){
      $("#rawData").html(syntaxHighlight(JSON.stringify(JSON.parse(data).raw, undefined, 4 )));
        $("#fhirdata").html(syntaxHighlight(JSON.stringify(JSON.parse(data).fhirdata,undefined, 4 )));
    });
    function syntaxHighlight(json) {
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    }

});
