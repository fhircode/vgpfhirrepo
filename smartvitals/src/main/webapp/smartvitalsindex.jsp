<%@ page import="org.apache.http.client.methods.HttpGet" %>
<%@ page import="org.apache.http.client.HttpClient" %>
<%@ page import="org.apache.http.impl.client.HttpClients" %>
<%@ page import="org.apache.http.HttpResponse" %>
<%@ page import="org.apache.http.HttpEntity" %>
<%@ page import="org.apache.http.util.EntityUtils" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="org.apache.http.client.methods.HttpPost" %>
<%@ page import="org.apache.http.NameValuePair" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.apache.http.message.BasicNameValuePair" %>
<%@ page import="org.apache.http.client.entity.UrlEncodedFormEntity" %>
<%@ page import="org.smartvitals.AppDetails" %>
<html>
<body>
<script>
    var globalParam = <%= (AppDetails.getDetailsFromSession(session).getDetailsInfo())%>;
</script>
<style>
    .componentStyle {
        border: 1px solid #ADA7A7;
        border-radius: 5px;
        padding: 0px;
        padding-right: 8px;
        padding-left: 8px;
        display: inline-block;
        box-shadow: 3px 3px #E6E6E6;
    }
    h3.compheader {
        margin: 0px;
        background: #C7C7C7;
        color: #6F6C6C;
        margin-left: -8px;
        margin-right: -8px;
        padding: 2px;
        padding-left: 8px;
        padding-right: 8px;
        padding-bottom: 5px;
        text-shadow: 2px 1px #E0E0E0;
        border-bottom: 1px solid #ADA7A7;
    }
    .compcontent {
        padding: 5px;
        padding-bottom: 8px;
    }

    #fhirdata{
        height:auto;
        width:50%;
        margin:2px;
        padding:3px;
    }
    #rawData{
        height:auto;
        width:50%;
        margin:2px;
        padding:3px;
    }
    pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; }
    .string { color: green; }
    .number { color: darkorange; }
    .boolean { color: blue; }
    .null { color: magenta; }
    .key { color: red; }
</style>
<script src="js/jquery-ui-1.11.4.custom/external/jquery/jquery.js"></script>
<script src="js/main.js"></script>
<div class="componentStyle">
    <h3 class="compheader">Blood Pressure</h3>
    <div class="compcontent">
        <div class="componentStyle">
            <h3 class="compheader">Raw data from data provider</h3>
            <div id="rawData">

            </div>
        </div>
        <div class="componentStyle">
            <h3 class="compheader">Data Converted using HAPI fhir</h3>
            <div id="fhirdata">

            </div>
        </div>
    </div>
</div>
<div>

</div>
</body>
</html>
